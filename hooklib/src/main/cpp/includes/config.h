//
// Created by liumeng on 2020/10/12.
//

#ifndef SANDHOOK_CONFIG_H
#define SANDHOOK_CONFIG_H
#define CLASS_SAND_HOOK "com/swift/sandhook/SandHook"
#define CLASS_NEVER_CALL "com/swift/sandhook/ClassNeverCall"
#define  ClASS_PH "com/swift/sandhook/PendingHookHandler"
#define ClASS_SMR "com/swift/sandhook/SandHookMethodResolver"
#define ClASS_AMSTX "com.swift.sandhook.ArtMethodSizeTest"
#define ClASS_AMST "com/swift/sandhook/ArtMethodSizeTest"
#define CLASS_NEVER_CALLX "com.swift.sandhook.ClassNeverCall"
#define CLASS_CONFIG "com/swift/sandhook/SandHookConfig"
#ifdef __LP64__
#define LIB_BASE_PATH "/system/lib64/"
#else
#define LIB_BASE_PATH "/system/lib/"
#endif
#endif //SANDHOOK_CONFIG_H
